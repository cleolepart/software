{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "4dcf1f30",
   "metadata": {},
   "outputs": [],
   "source": [
    "import qubic.toolchain as tc\n",
    "import qubic.rpc_client as rc\n",
    "import qubitconfig.qchip as qc\n",
    "from distproc.hwconfig import FPGAConfig, load_channel_configs\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2855f010",
   "metadata": {},
   "source": [
    "# Load Configs and Define Circuit"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "053e4b2b",
   "metadata": {},
   "source": [
    "define FPGA config; this has timing information for the scheduler. For now it is fine to use the default config"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "f5c7a0c4",
   "metadata": {},
   "outputs": [],
   "source": [
    "fpga_config = FPGAConfig()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "92681d85",
   "metadata": {},
   "source": [
    "load channel configs (firmware channel mapping + configuration, see [Understanding Channel Configuration](https://gitlab.com/LBL-QubiC/software/-/wikis/Understanding-Channel-Configuration) for details), and QChip object, which contains calibrated gates + readout.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "89e39c66",
   "metadata": {},
   "outputs": [],
   "source": [
    "channel_configs = load_channel_configs('channel_config.json')\n",
    "qchip = qc.QChip('qubitcfg.json')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d153cd95",
   "metadata": {},
   "source": [
    "As an alternative to the above, if you're using the chipcalibration repository, you can load all three configs like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "39c97603",
   "metadata": {},
   "outputs": [],
   "source": [
    "import chipcalibration.config as cfg\n",
    "chipname = 'X4Y2' #this is a folder in the 'qchip' submodule of chipcalibration, containing the name of your chip\n",
    "fpga_config, qchip, channel_config = cfg.load_configs(chipname)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9284590-2c7e-46b6-b54d-a982a8cb3e56",
   "metadata": {},
   "source": [
    "# `branch_fproc` Instruction\n",
    "\n",
    "The instruction used for **branching** looks like this:\n",
    "\n",
    "```json\n",
    "{'name': 'branch_fproc', 'alu_cond': <'le' or 'ge' or 'eq'>, 'cond_lhs': <var or ival>, \n",
    "'func_id': function_id, 'scope': <list_of_qubits_or_channels> 'true': [instruction_list], 'false': [instruction_list]}\n",
    "```\n",
    "\n",
    "Let's break this down:\n",
    " 1. Data is **requested** from the FPROC according to the provided `func_id`. In the version of the gateware we're simulating, `func_id` just indicates the qubit whose measurement result we want. A list of available FPROC channels can be found in `fpga_config.fproc_channels`\n",
    " 2. Once the FPROC receives the request, it fetches the result of the **most recent previous measurement** on that channel and sends it to the **core(s)** that requested it\n",
    " 3. The **core** makes a **branching decision** according to `cond_lhs <alu_cond> fproc_result`. For example, you can check if the measurement was 0 using: `cond_lhs = 0`, `alu_cond = 'eq'`, which implements: `0 == fproc_result`.\n",
    " 4. If the expression evaluates to **True**, the block of instructions in the `true` field are executed, **else** the block in `false` is executed.\n",
    "\n",
    "## A note about state classification/thresholding\n",
    "\n",
    "The FPROC classsifies states by thresholding across the y-axis; any accumulated value with `x>0` gets classified to 0; `x<0` goes to 1. Future FPROC implementations will include custom thresholds and qutrit states.\n",
    "\n",
    "![image](./reset_image.svg)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "943b64aa-e915-420e-b2c4-c53f77618ac7",
   "metadata": {},
   "source": [
    "## FPROC Channels\n",
    "\n",
    "A list of available FPROC channels in the current gateware can be found in `fpga_config.fproc_channels`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "69838b66-446d-4e66-a7df-4b40e5a71f96",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'Q0.meas': FPROCChannel(id=('Q0.rdlo', 'core_ind'), hold_after_chans=['Q0.rdlo'], hold_nclks=64),\n",
       " 'Q1.meas': FPROCChannel(id=('Q1.rdlo', 'core_ind'), hold_after_chans=['Q1.rdlo'], hold_nclks=64),\n",
       " 'Q2.meas': FPROCChannel(id=('Q2.rdlo', 'core_ind'), hold_after_chans=['Q2.rdlo'], hold_nclks=64),\n",
       " 'Q3.meas': FPROCChannel(id=('Q3.rdlo', 'core_ind'), hold_after_chans=['Q3.rdlo'], hold_nclks=64),\n",
       " 'Q4.meas': FPROCChannel(id=('Q4.rdlo', 'core_ind'), hold_after_chans=['Q4.rdlo'], hold_nclks=64),\n",
       " 'Q5.meas': FPROCChannel(id=('Q5.rdlo', 'core_ind'), hold_after_chans=['Q5.rdlo'], hold_nclks=64),\n",
       " 'Q6.meas': FPROCChannel(id=('Q6.rdlo', 'core_ind'), hold_after_chans=['Q6.rdlo'], hold_nclks=64),\n",
       " 'Q7.meas': FPROCChannel(id=('Q7.rdlo', 'core_ind'), hold_after_chans=['Q7.rdlo'], hold_nclks=64)}"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fpga_config.fproc_channels"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "612e7d51-011e-4543-bf8a-138ecf4afdbb",
   "metadata": {},
   "source": [
    "Each named channel indexes a `FPROCChannel` object, which contains the physical channel `id` to use, as well as the additional timing paremeters.\n",
    "\n",
    "The channel `id` is resolved by the assembler using the `channel_configs` object; e.g. for `Q0.meas`, the physical channel ID is given by `channel_configs['Q0.rdlo'].core_ind`.\n",
    "\n",
    "`hold_after_chans` and `hold_nclks` control how much delay is added before executing the branch instruction. More specifically, the compiler tools will ensure that there are at least `hold_nclks` clock cycles between the end of the most recent pulse on the channels in `hold_after_chans` and the execution of the branch (or `read_fproc`) instruction. This is to give the FPGA enough time to process the measurement result and store it in the FPROC memory. Delays are added to the program using `idle` instructions, which halt processor execution until the specified timestamp (which is referenced to the same counter as the pulse `start_time`).\n",
    "\n",
    "In the current gateware, the feedback latency is given by:\n",
    "64 (clocks between end of `rdlo` pulse and start of branch instruction) + 8 (clocks to execute branch instruction) + 3 (clocks to load next pulse) = 75 clocks (or 150 ns)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b68d5eb-5fa4-4644-9f14-d9da186ee052",
   "metadata": {},
   "source": [
    "# Example: Conditional Bit-flip Circuit"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb55d32c-0b67-42fb-8023-a445e4dea5a1",
   "metadata": {},
   "source": [
    "This circuit applies a bit flip on `Q0` conditioned on the classical measurement result from `Q1`. Note that no delays need to be inserted between the `read` on `Q1` and the pulses applied to `Q0`; this is resolved by the compiler according to the `FPROCChannel` objects in the `FPGAConfig`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "f8c53a13-cdb2-4cad-96b6-4e820c644250",
   "metadata": {},
   "outputs": [],
   "source": [
    "circuit = [\n",
    "    {'name': 'delay', 't': 400.e-6, 'qubit': ['Q0', 'Q1']},\n",
    "    {'name': 'X90', 'qubit': ['Q1']},\n",
    "    {'name': 'read', 'qubit': ['Q1']},\n",
    "    {'name': 'branch_fproc', 'alu_cond': 'eq', 'cond_lhs': 1, 'func_id': 'Q1.meas', 'scope': ['Q0'],\n",
    "                'true': [\n",
    "                            {'name': 'X90', 'qubit': ['Q0']}, \n",
    "                            {'name': 'X90', 'qubit': ['Q0']},\n",
    "\n",
    "                ],\n",
    "                'false': []},\n",
    "    {'name': 'barrier', 'qubit':['Q0', 'Q1']},\n",
    "    {'name': 'read', 'qubit': ['Q0']},\n",
    "    {'name': 'read', 'qubit': ['Q1']},\n",
    "\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4eaeeeb3",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Compile and Assemble\n",
    "\n",
    "Compile the program. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "c9867122",
   "metadata": {},
   "outputs": [],
   "source": [
    "compiled_prog = tc.run_compile_stage(circuit, fpga_config, qchip)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0581293",
   "metadata": {},
   "source": [
    "Run the assembler to convert the above program into machine code that we can load onto the FPGA:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "f970c778",
   "metadata": {},
   "outputs": [],
   "source": [
    "raw_asm = tc.run_assemble_stage(compiled_prog, channel_configs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "657a52b9",
   "metadata": {},
   "source": [
    "## Connect to Server and Run Circuit\n",
    "\n",
    "Now that we've defined our circuit and compiled it to machine code, we can submit it to the ZCU216 and run it.\n",
    "\n",
    "Instantiate the runner client:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c838ce41",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
