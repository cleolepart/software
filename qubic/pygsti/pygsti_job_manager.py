from qubic.job_manager import job_manager as _jm
from collections import OrderedDict
import pygsti

class PyGSTiJobManager(_jm.JobManager):
    def __init__(self, fpga_config, channel_configs, circuit_runner, qchip,
                 gmm_manager, ordered_register, target_platform='rfsoc'):
        self.register = ordered_register
        if gmm_manager is None:
            raise(ValueError("PyGSTiJobManager needs a gmm_manager!"))
        super().__init__(fpga_config, channel_configs, circuit_runner, qchip,
                 gmm_manager, target_platform)

    def collect_dataset(self, job_dict, num_shots_per_circuit, qchip):
        if not isinstance(job_dict, (dict, OrderedDict)):
            raise(ValueError("the jobs should be a dictionary"))
        shots = self.collect_classified_shots(list(job_dict.values()), num_shots_per_circuit, qchip=qchip)


        ds = pygsti.data.DataSet(outcome_labels=[bin(i)[2:].zfill(3) for i in range(2**3)])
        for id_circ, circ in enumerate(job_dict.keys()):
            counts = dict()
            for id_shot in range(num_shots_per_circuit):
                bits = ''
                for qid in register:
                    bits += str(shots[qid][id_circ][id_shot][0])
                if bits in counts:
                    counts[bits] += 1
                else:
                    counts[bits] = 1
            ds[circ] = counts
        return ds