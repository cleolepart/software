import distproc.compiler as cm
import distproc.assembler as am
import qubic.rfsoc.hwconfig as hw
import qubitconfig.qchip as qc
import json
import os
from typing import List, Dict

def run_compile_stage(program: List, fpga_config: hw.FPGAConfig, qchip: qc.QChip, 
                      compiler_flags: Dict[str, bool] | cm.CompilerFlags = None) -> cm.CompiledProgram | List[cm.CompiledProgram]:
    """
    Wrapper around distributed processor compiler stage. 

    Parameters
    ----------
    program: list
        single QubiC program or list of programs. Each program can be:

            - list of dicts formatted according to the QubiC Higher-level 
              Representation
            - list of QubiC intermediate representation instructions 
              (distproc.ir_instruction classes)
    fpga_config: hw.FPGAConfig
    qchip: qc.QChip
    compiler_flags: dict | cm.CompilerFlags
        Options for configuring the compiler; see distproc.compiler.get_passes for 
        more details. Default is None, which sets all flags to True (fine for 
        most use cases)

    Returns
    -------
    CompiledProgram or List[CompiledProgram]
        object containing compiled program
    """
    passes = cm.get_passes(fpga_config, qchip, compiler_flags=compiler_flags)
    if isinstance(program[0], dict):
        compiler = cm.Compiler(program)
        compiler.run_ir_passes(passes)
        return compiler.compile()
    elif isinstance(program[0], list):
        compiled_progs = []
        for circuit in program:
            compiler = cm.Compiler(circuit)
            compiler.run_ir_passes(passes)
            compiled_progs.append(compiler.compile())
        return compiled_progs
    else:
        raise TypeError

def run_assemble_stage(compiled_program: cm.CompiledProgram | List[cm.CompiledProgram], 
                       channel_configs: Dict[str, hw.ChannelConfig], target_platform: str = 'rfsoc') -> Dict | List[Dict]:
    """
    Wrapper around distributed processor assembler stage. 

    Parameters
    ----------
    compiled_program: CompiledProgram | List[CompiledProgram]
    channel_configs: hw.ChannelConfig
    target_platform: str
        target hardware platform; currently only 'rfsoc' is supported

    Returns
    -------
    dict or list of dict
        dict(s) containing the assembled binaries 
    """
    if target_platform != 'rfsoc':
        raise Exception('rfsoc is currently the only supported platform!')

    if isinstance(compiled_program, list):
        raw_asm_progs = []
        for prog in compiled_program:
            asm = am.GlobalAssembler(prog, channel_configs, hw.RFSoCElementCfg)
            raw_asm_progs.append(asm.get_assembled_program())
        return raw_asm_progs

    else:
        asm = am.GlobalAssembler(compiled_program, channel_configs, hw.RFSoCElementCfg)
        return asm.get_assembled_program()

