from distproc.hwconfig import ChannelConfig, ElementConfig, \
    FPGAConfig, load_channel_configs
import qubitconfig.envelope_pulse as ep
import distproc.command_gen as cg
from typing import List, Dict

try:
    import ipdb
except ImportError:
    print('warning: failed to import ipdb')
import numpy as np

class RFSoCElementCfg(ElementConfig):
    """
    ElementConfig implementation for QubiC 2.0 on ZCU216.
    """
    def __init__(self, samples_per_clk: int = 16, interp_ratio: int = 1, env_max_samples: int = 4095):
        self.env_n_bits = 16
        self.freq_n_bits = 32
        self.n_phase_bits = 17
        self.amp_n_bits = 16
        self.interp_ratio = interp_ratio
        self.env_max_samples = env_max_samples
        super().__init__(2.e-9, samples_per_clk)

    def get_freq_addr(self, freq_ind: int) -> int:
        return freq_ind

    def get_cfg_word(self, elem_ind: int, mode_bits: int = None) -> int:
        if mode_bits is not None: 
            raise Exception('mode not implemented')
        return elem_ind

    def get_freq_buffer(self, freqs: list | np.ndarray) -> np.ndarray:
        """
        Converts a list of frequencies (in Hz) to a buffer, where each frequency 
        has 16 elements:

          - `[0]` is a 32-bit freq word, encoding phase increment per clock cycle
          - `[1:15]` are 16 bit I MSB + 16 bit Q LSB, encoding 15 phase offsets for 
                each sample (except 0) within a clock cycle
        16-element arrays for each frequency are concatenated into a single 1D numpy array.

        Parameters
        ----------
        freqs: list | np.ndarray
            List of frequencies, in Hz

        Returns
        -------
        np.ndarray:
            Frequency buffer: array of concatenated 16-element frequency lists; shape
            is `(len(freqs)*16,)`

        """
        freq_buffer = np.empty(0)
        scale = 2**(self.freq_n_bits/2 - 1) - 1
        for freq in freqs:
            cur_freq_buffer = np.zeros(self.samples_per_clk)
            if freq is not None:
                cur_freq_buffer[0] = int(freq*2**self.freq_n_bits/self.fpga_clk_freq) & (2**self.freq_n_bits - 1)
                for i in range(1, self.samples_per_clk):
                    i_mult = int(round(np.cos(2*np.pi*freq*i*self.sample_period)*scale) % (2**(self.freq_n_bits/2)))
                    q_mult = int(round(np.sin(2*np.pi*freq*i*self.sample_period)*scale) % (2**(self.freq_n_bits/2)))
                    cur_freq_buffer[i] = (i_mult << (self.freq_n_bits//2)) + q_mult

            freq_buffer = np.append(freq_buffer, cur_freq_buffer)

        return freq_buffer

    def get_phase_word(self, phase: float) -> int:
        """
        Converts phase to 17 bit unsigned int, normalized to 2*pi
        (i.e. 2\*pi -> 2\*\*17)
        Parameters
        ----------
        phase: float
            input phase (in radians)

        Returns
        -------
        int:
            17-bit phase word
        """
        phase = int(((phase % (2*np.pi))/(2*np.pi)) * 2**17)
        return phase % 2**17

    def get_env_word(self, env_ind: int, length_nsamples: int) -> int:
        """
        Returns the envelope word stored in the pulse command, which encodes the
        starting address and length of the pulse envelope.

        Parameters
        ----------
        env_ind: int
            starting index of the envelope in the envelope buffer
        length_nsamples: int
            length of the envelope in samples (could be the same as the
            pulse length in samples, or lower if interpolating)
        Returns
        -------
        int:
            env_word
        """
        if env_ind + length_nsamples > self.env_max_samples:
            raise Exception('{} exceeds max env length of {}'.format(env_ind + length_nsamples, self.env_max_samples))
        return env_ind//int(self.samples_per_clk/self.interp_ratio) \
                + (int(np.ceil(self.interp_ratio*length_nsamples/self.samples_per_clk)) << 12)

    def get_cw_env_word(self, env_ind: int) -> int:
        """
        Returns the envelope word for a CW pulse. `env_ind` is required 
        since the CW pulse requires a single clock cycle of envelope
        data to be stored.

        Parameters
        ----------
        env_ind: int
            starting index of the envelope in the envelope buffer
        Returns
        -------
        int:
            env_word
        """
        if self.samples_per_clk//self.interp_ratio > self.env_max_samples:
            raise Exception('{} exceeds max env \
                            length of {}'.format(env_ind + self.samples_per_clk//self.interp_ratio, self.env_max_samples))
        return env_ind//int(self.samples_per_clk/self.interp_ratio)
                

    def get_env_buffer(self, env: np.ndarray | list | dict):
        """
        Converts env to a list of samples to write to the env buffer memory.

        Parameters
        ----------
        env : np.ndarray, list, or dict
            if np.ndarray or list this is interpreted as a list of samples. Samples
            should be normalized to 1.

            if dict, a function in the qubitconfig.envelope_pulse library is used to
            calculate the envelope samples. env['env_func'] should be the name of the function,
            and env['paradict'] is a dictionary of attributes to pass to env_func. The 
            set of attributes varies according to the function but should include the 
            pulse duration twidth

        Returns
        -------
        np.ndarray:
            buffer of envelope data

        """
        if isinstance(env, np.ndarray) or isinstance(env, list):
            env_samples = np.asarray(env)
        elif isinstance(env, dict):
            dt = self.interp_ratio * self.sample_period
            env_func = getattr(ep, env['env_func'])
            _, env_samples = env_func(dt=dt, **env['paradict'])
            # ipdb.set_trace()
        elif env == 'cw':
            env_samples = np.ones(self.samples_per_clk//self.interp_ratio)
        else:
            raise TypeError(f'env {env} must be dict or array')

        env_samples = np.pad(env_samples, (0, (self.samples_per_clk//self.interp_ratio - len(env_samples)
                % self.samples_per_clk//self.interp_ratio) % self.samples_per_clk//self.interp_ratio))

        return (cg.twos_complement(np.real(env_samples*(2**(self.env_n_bits-1)-1)).astype(int), nbits=self.env_n_bits) << self.env_n_bits) \
                    + cg.twos_complement(np.imag(env_samples*(2**(self.env_n_bits-1)-1)).astype(int), nbits=self.env_n_bits)

    def length_nclks(self, tlength: float) -> int:
        """
        Converts pulse length in seconds to integer number of clock cycles.

        Parameters
        ----------
        tlength: float
            time in seconds

        Returns
        -------
        int:
            time in clocks
        """
        return int(np.ceil(tlength/self.fpga_clk_period))

    def get_amp_word(self, amplitude: float) -> int:
        """
        Converts amplitude (normalized to 1) into command word for FPGA

        Parameters
        ----------
        amplitude: float

        Returns
        -------
        int:
            amplitude word
            
        """
        return int(cg.twos_complement(amplitude*(2**(self.amp_n_bits - 1) - 1), nbits=self.amp_n_bits))

