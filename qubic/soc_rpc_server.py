import xmlrpc.server
from qubic.run import CircuitRunner
import logging
import argparse
from functools import partial, update_wrapper

def run_soc_server(ip: str, port: int, xsa_commit: str, 
                   enable_local_batching: bool = False):
    """
    Start an xmlrpc server that exposes an instance of CircuitRunner over a 
    network. Intended to be run from the RFSoC ARM core python (pynq) 
    environment. IP should only be accessible locally!

    Parameters
    ----------
    ip : str
    port : int
    xsa_commit : str
    enable_local_batching : bool
        if True, allow clients to submit batches of circuits directly to 
        the SOC (bypassing the JobServer). Default is False.
    """
    runner = CircuitRunner(commit=xsa_commit)

    server = xmlrpc.server.SimpleXMLRPCServer((ip, port), logRequests=True, allow_none=True)

    server.register_function(runner.load_circuit)
    server.register_function(runner.run_circuit)
    server.register_function(runner.load_and_run_acq)

    if enable_local_batching:
        run_circuit_batch = partial(runner.run_circuit_batch, from_server=True)
        update_wrapper(run_circuit_batch, runner.run_circuit_batch)
        server.register_function(run_circuit_batch)

    print('RPC server running on {}:{}'.format(ip, port))

    server.serve_forever()

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--ip', default='192.168.1.247')
    parser.add_argument('--port', default=9095, type=int)
    parser.add_argument('--xsa-commit', default='024756ff')
    parser.add_argument('--log-level', default=None)
    parser.add_argument('--enable-batching', action='store_true')
    args = parser.parse_args()

    print(f'starting RPC Server on {args.ip}:{args.port}')

    if args.log_level is not None:
        if args.log_level.lower() == 'info':
            logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s')

        elif args.log_level == 'debug':
            logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(message)s')

        else:
            raise Exception(f'log level: {args.log_level} not supported')

    run_soc_server(args.ip, int(args.port), args.xsa_commit, args.enable_batching)


